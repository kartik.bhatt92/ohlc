package com.analytics.controller;

import java.io.*;
import java.nio.file.*;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.*;

import org.slf4j.Logger;

import com.analytics.entity.Trade;
import com.analytics.entity.TradeOLHC;

public class ProcessTrade extends Thread {
	private static final long MAXALLOWED = 15;
	private BlockingQueue<Trade> consume;
	private BlockingQueue<TradeOLHC> produce;
	private static int COUNTER = 1;
	Logger logger = org.slf4j.LoggerFactory.getLogger(ProcessTrade.class);

	public ProcessTrade(BlockingQueue<Trade> consume, BlockingQueue<TradeOLHC> produce) {
		this.consume = consume;
		this.produce = produce;

	}

	public void run() {
		boolean flag=false;
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while (true) {
			try {

				Trade trade = consume.peek();

				if (trade != null) {
					trade = consume.take();
					parseFile(trade);
					flag=true;
					
				}
				else {
					if(flag)
					break;
				}
					
				
			} catch (InterruptedException | IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	private void parseFile(Trade trade) throws IOException {

		double price = trade.getP();
		double quantity = trade.getQ();
		TradeOLHC tradeOLHC = new TradeOLHC();

		if (TradeOLHC.getClose() > 0) {
				reset();
		}

		if (TradeOLHC.initalTimestamp == Long.MIN_VALUE) {
			TradeOLHC.initalTimestamp = trade.getTS2();
			//TradeOLHC.nextTimestamp=TradeOLHC.initalTimestamp+TimeUnit.SECONDS.toNanos(15);
		}
		if(TradeOLHC.nextTimestamp==Long.MIN_VALUE)
		TradeOLHC.nextTimestamp=TradeOLHC.initalTimestamp+TimeUnit.SECONDS.toNanos(15);
		
		
		if (isIntervalComplete(TradeOLHC.nextTimestamp,trade.getTS2())) {
			System.out.println("no trades");
			TradeOLHC.setBar_num(COUNTER++);
			long temp=TradeOLHC.initalTimestamp;
			TradeOLHC.initalTimestamp = TradeOLHC.nextTimestamp;
			TradeOLHC.nextTimestamp=temp+TimeUnit.SECONDS.toNanos(15);
		}

		if (TradeOLHC.getOpen() == 0) {
			TradeOLHC.setOpen(price);
		}

		if (TradeOLHC.getLow() == 0 || TradeOLHC.getLow() > price) {
			TradeOLHC.setLow(price);
		}
		if (TradeOLHC.getHigh() == 0 || TradeOLHC.getHigh() < price) {
			TradeOLHC.setHigh(price);
		}

		TradeOLHC.setVolume(TradeOLHC.getVolume() + quantity);
		tradeOLHC.setEvent("ohlc_notify");
		tradeOLHC.setSymbol(trade.getSym());
		TradeOLHC.setBar_num(COUNTER);
		
		if (!Objects.isNull(consume.peek()) && isDue(TradeOLHC.initalTimestamp, consume.peek().getTS2())) {

			TradeOLHC.setClose(trade.getP());
		}

		System.out.println(tradeOLHC.toString());
	//	HashMap<String, LinkedHashMap<Integer, List<TradeOLHC>>> hm = new HashMap<>();
		// logger.info(tradeOLHC.toString());
	}

	private boolean isIntervalComplete(long timeStamp1, long timeStamp2 ) {

		if (timeStamp1 < timeStamp2)
			return true;


		return false;
	}

	private void reset() {

		TradeOLHC.setClose(0);
		TradeOLHC.setBar_num(COUNTER++);
		TradeOLHC.setOpen(0);
		TradeOLHC.setLow(0);
		TradeOLHC.setHigh(0);
		TradeOLHC.initalTimestamp = Long.MIN_VALUE;
		TradeOLHC.setVolume(0);
		
	}

	private boolean isDue(long timeStamp1, long timeStamp2) {

		if (timeStamp1 == timeStamp2)
			return false;

		Instant instant1 = Instant.ofEpochSecond(0L, timeStamp1);

		Instant instant2 = Instant.ofEpochSecond(0L, timeStamp2);

		Duration res = Duration.between(instant1, instant2);
		if (res.getSeconds() > MAXALLOWED)
			return true;

		return false;
	}
}
