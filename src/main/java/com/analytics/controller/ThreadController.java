package com.analytics.controller;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;

import org.springframework.stereotype.Controller;

import com.analytics.entity.Trade;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This producer enumerates files of a specific type in a given directory and
 * then put the files on the queue.
 *
 * @author www.codejava.net
 */
@Controller
public class ThreadController extends Thread {
	private BlockingQueue<Trade> queue;
	private String directory;

	public ThreadController(BlockingQueue<Trade> queue, String directory) {
		this.queue = queue;
		this.directory = directory;

	}

	public void run() {

		try {
			listDirectory(directory);
			// queue.put(new File("END"));

		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}
	}

	private void listDirectory(String dir) throws InterruptedException {
		ObjectMapper mapper = new ObjectMapper();
		Path pathRaw = Paths.get(dir);
		
		

		try (BufferedReader br = Files.newBufferedReader(pathRaw, StandardCharsets.UTF_8)) {

			String line;

			while ((line = br.readLine()) != null) {

				Trade melon = mapper.readValue(line, Trade.class);

			//	System.out.println("Current melon is: " + melon.toString());
				if(melon.getSym().equals("XXBTZUSD"))
				queue.put(melon);

			}

		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}

	}

}