package com.analytics.entity;

import org.springframework.stereotype.Component;

@Component
public class TradeOLHC {


	
	private static double open=0;
	private static double high=0;
	private static double low=0;
	private static double close=0;
	private static double volume=0 ;
	private String event ;
	private String symbol;
	private static int bar_num=1;
	public static long initalTimestamp=Long.MIN_VALUE;
	public static long nextTimestamp=Long.MIN_VALUE;
	
	public static double getOpen() {
		return open;
	}
	public static void setOpen(double open) {
		TradeOLHC.open = open;
	}
	public static double getHigh() {
		return high;
	}
	public static void setHigh(double high) {
		TradeOLHC.high = high;
	}
	public static double getLow() {
		return low;
	}
	public static void setLow(double low) {
		TradeOLHC.low = low;
	}
	public static double getClose() {
		return close;
	}
	public static void setClose(double close) {
		TradeOLHC.close = close;
	}
	public static double getVolume() {
		return volume;
	}
	public static void setVolume(double volume) {
		TradeOLHC.volume = volume;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public static int getBar_num() {
		return bar_num;
	}
	public static void setBar_num(int bar_num) {
		TradeOLHC.bar_num = bar_num;
	}
	@Override
	public String toString() {
		return "TradeOLHC [event=" + event + ", symbol=" + symbol + ", open=" + open
				+", close=" + close +
				", high=" + high+
				", low=" + low+ 
				", volume=" + volume+
				", bar_num=" + bar_num+"]";
	}
	

	
}
