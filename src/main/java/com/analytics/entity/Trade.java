package com.analytics.entity;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

@Component
public class Trade {
	
	
	
	private String sym;
	private String T;
	private double P ;
	private double Q ;
	private long TS;
	private String side;
	private long TS2;
	
	
	public String getSym() {
		return sym;
	}
	public void setSym(String sym) {
		this.sym = sym;
	}
	public String getT() {
		return T;
	}
	@JsonProperty("T")
	public void setT(String t) {
		T = t;
	}
	
	
	@Override
	public String toString() {
		return "Trade [sym=" + sym + ", T=" + T + ", P=" + P + ", Q=" + Q + ", TS=" + TS + ", side=" + side + ", TS2="
				+ TS2 + "]";
	}
	public double getP() {
		return P;
	}
	
	@JsonProperty("P")
	public void setP(double p) {
		P = p;
	}
	public double getQ() {
		return Q;
	}
	
	@JsonProperty("Q")
	public void setQ(double q) {
		Q = q;
	}
	public long getTS() {
		return TS;
	}
	
	@JsonProperty("TS")
	public void setTS(long tS) {
		TS = tS;
	}
	public String getSide() {
		return side;
	}
	public void setSide(String side) {
		this.side = side;
	}
	
	
	public long getTS2() {
		return TS2;
	}
	
	@JsonProperty("TS2")
	public void setTS2(long tS2) {
		TS2 = tS2;
	}
	/*
	 * public Trade(String sym, String t, double p, double q, long tS, String side,
	 * long tS2) { super(); this.sym = sym; T = t; P = p; Q = q; TS = tS; this.side
	 * = side; TS2 = tS2; }
	 */
	
	

}
