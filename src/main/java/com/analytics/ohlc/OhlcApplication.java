package com.analytics.ohlc;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.analytics.controller.ProcessTrade;
import com.analytics.controller.ThreadController;
import com.analytics.entity.Trade;
import com.analytics.entity.TradeOLHC;

@SpringBootApplication
public class OhlcApplication {

	@Autowired
	static ThreadController lister;

	@Autowired
	static ProcessTrade consumer;

	public static void main(String[] args) {
		SpringApplication.run(OhlcApplication.class, args);

		BlockingQueue<Trade> queueIn = new ArrayBlockingQueue<>(100);
		BlockingQueue<TradeOLHC> queueOut = new ArrayBlockingQueue<>(100);
		lister = new ThreadController(queueIn, "C:\\Users\\kb\\Downloads\\trades-data\\trades.json");
		lister.start();

		consumer = new ProcessTrade(queueIn, queueOut);
		consumer.start();

	}
}
